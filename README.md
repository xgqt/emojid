# emojid 🤣

Yes, that's the actual name!
For sake of our sanity the repo name,
source and created binaries don't contain any emoji.

<p align="center">
    <a href="https://gitlab.com/xgqt/emojid">
        <img src="./emojid.png">
    </a>
</p>

# Abstract

detect if OS is running systemd,
if so, log to journald "Your 👨 system 💿️ is broken 😭 get 🤑 yourself 👨 a better 🤟 init 🚀",
else log "This system appears to be ran by a sane person"


# Why?

Why the living hell does this exist?

- https://github.com/systemd/systemd/commit/a8b627aaed409a15260c25988970c795bf963812#diff-03b3e8b6554bb8ccd539ad2e547d9ef13f80428101bdc01b4d6e9ea5f685fe7cR1181
- https://github.com/systemd/systemd/commit/a8b627aaed409a15260c25988970c795bf963812#diff-03b3e8b6554bb8ccd539ad2e547d9ef13f80428101bdc01b4d6e9ea5f685fe7cR1204


# WARNING

THIS IS A JOKE
Maybe I just have shitty humor ;)


# License

GPL v3 as always
