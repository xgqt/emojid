/*************************************************
 * Copyright (c) 2020 XGQT <xgqt@riseup.net> *
 * Licensed under the GNU GPL v3 license         *
 *************************************************/


#include <dirent.h>
#include <errno.h>
#include <stdlib.h>
#include <syslog.h>
#include <unistd.h>


void log_bad ()
{
    syslog(
           LOG_INFO,
           "Your 👨 system 💿️ is broken 😭 get 🤑 yourself 👨 a better 🤟 init 🚀"
           );
}


int main ()
{
    while (1) {

        openlog("emojid", LOG_PID, LOG_USER);

        DIR * dir = opendir("/run/systemd/system/");

        if (dir) {
            log_bad();
            closedir(dir);
        }
        else if (! system("which systemd >/dev/null 2>&1")) {
            log_bad();
        }
        else {
            syslog(
                   LOG_INFO,
                   "This system appears to be ran by a sane person, good for You!"
                   );
        }

        closelog();

        sleep(666);
    }

    return 0;
}
